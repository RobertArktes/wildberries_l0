module gitlab.com/RobertArktes/wildberries_l0

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/jackc/pgx/v4 v4.15.0
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/nats-server v1.4.1 // indirect
	github.com/nats-io/nats.go v1.13.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/spf13/viper v1.10.1
	go.uber.org/zap v1.21.0
)
