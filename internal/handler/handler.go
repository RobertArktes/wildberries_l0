package handler

import (
	"github.com/go-chi/chi/v5"
	"github.com/nats-io/nats.go"
	"github.com/patrickmn/go-cache"
	"gitlab.com/RobertArktes/wildberries_l0/internal/infrastructure/postgres"
)

type Handler struct {
	repos *postgres.Repository
	nc    *nats.EncodedConn
	cache *cache.Cache
}

func NewHandler(repos *postgres.Repository, nc *nats.EncodedConn, c *cache.Cache) *Handler {
	return &Handler{
		repos: repos,
		nc:    nc,
		cache: c,
	}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()

	h.natsHandler()

	router.Route("/", func(r chi.Router) {
		r.Get("/data", h.GetData)
		r.Post("/data_response", h.DataResponse)
	})

	return router
}
