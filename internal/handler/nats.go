package handler

import (
	"encoding/json"
	"time"

	"github.com/nats-io/nats.go"
	"gitlab.com/RobertArktes/wildberries_l0/internal/models"
	"go.uber.org/zap"
)

func (h *Handler) natsHandler() {
	h.nc.Subscribe("data", func(m *nats.Msg) {
		data := &models.Data{}
		err := json.Unmarshal(m.Data, data)
		if err != nil {
			zap.L().Error("error unmarshaling data", zap.Error(err))

			return
		}

		h.cache.Set(data.Order_uid, data, 10*time.Minute)

		err = h.repos.InsertData(data)
		if err != nil {
			zap.L().Error("error insert data into postgres", zap.Error(err))

			return
		}

		zap.L().Info("insert data into postgres")
	})
}
